<?php require_once 'code.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP Basics and Selection Control Structures</title>
</head>
<body>
	<h2>Full Address:</h2>
	<p><?php echo getFullAddress("Philipines", "Quezon City", "Metro Manila", "3F Enzo Bldg."); ?></p>

	<h2>Letter-Based Grading</h2>
	<p><?php echo getLetterGrade(97); ?></p>
</body>
</html>